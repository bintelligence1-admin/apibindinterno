import { MigrationInterface, QueryRunner, getRepository } from 'typeorm';
import { User } from '../domain/user.entity';
import { Authority } from '../domain/authority.entity';

export class SeedUsersRoles1570200490072 implements MigrationInterface {
    role1: Authority = { name: 'ROLE_ADMIN' };

    role2: Authority = { name: 'ROLE_USER' };

    // user1: User = {
    //     login: 'system',
    //     password: 'system',
    //     firstName: 'System',
    //     lastName: 'System',
    //     email: 'system@localhost.it',
    //     imageUrl: '',
    //     activated: true,
    //     langKey: 'en',
    //     createdBy: 'system',
    //     lastModifiedBy: 'system',
    // };

    // user2: User = {
    //     login: 'anonymoususer',
    //     password: 'anonymoususer',
    //     firstName: 'Anonymous',
    //     lastName: 'User',
    //     email: 'anonymoususer@localhost.it',
    //     imageUrl: '',
    //     activated: true,
    //     langKey: 'en',
    //     createdBy: 'system',
    //     lastModifiedBy: 'system',
    // };

    user1: User = {
        login: 'admin',
        password: 'B1nt3ll1g3nc3',
        firstName: 'Administrator',
        lastName: 'Administrator',
        email: 'admin@localhost.it',
        imageUrl: '',
        activated: true,
        langKey: 'en',
        createdBy: 'system',
        lastModifiedBy: 'system',
    };

    user2: User = {
        login: 'odoo',
        password: 'odoo',
        firstName: 'Odoo',
        lastName: 'Odoo',
        email: 'odoo@localhost.it',
        imageUrl: '',
        activated: true,
        langKey: 'en',
        createdBy: 'system',
        lastModifiedBy: 'system',
    };

    user3: User = {
        login: 'bind',
        password: 'bind',
        firstName: 'Bind',
        lastName: 'Bind',
        email: 'bind@localhost.it',
        imageUrl: '',
        activated: true,
        langKey: 'en',
        createdBy: 'system',
        lastModifiedBy: 'system',
    };

    // eslint-disable-next-line
    public async up(queryRunner: QueryRunner): Promise<any> {
        const authorityRepository = getRepository('nhi_authority');

        const adminRole = await authorityRepository.save(this.role1);
        const userRole = await authorityRepository.save(this.role2);

        const userRepository = getRepository('nhi_user');

        this.user1.authorities = [adminRole, userRole];
        this.user2.authorities = [userRole];
        this.user3.authorities = [userRole];

        await userRepository.save([this.user1, this.user2, this.user3]);
    }

    // eslint-disable-next-line
    public async down(queryRunner: QueryRunner): Promise<any> {}
}
